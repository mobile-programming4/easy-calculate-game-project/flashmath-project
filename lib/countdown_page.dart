import 'dart:async';
import 'package:column_widget_example/game_page.dart';
import 'package:column_widget_example/selectmode_page.dart';
import 'package:flutter/material.dart';
import 'package:column_widget_example/const.dart';

class CountDownPage extends StatefulWidget {
  const CountDownPage({Key? key}) : super(key: key);

  @override
  State<CountDownPage> createState() => _CountDownPage();
}

class _CountDownPage extends State<CountDownPage> {
  int _secondsRemaining = 5;

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_secondsRemaining > 0) {
          _secondsRemaining--;
        } else {
          timer.cancel();
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
                return GamePage();
              }));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFC9FFFD),
      body: Center(
        child: Container(
          height: 250,
          width: 250,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(150),
            color: Colors.black,
          ),
          child: Center(
            child: Container(
                height: 230,
                width: 230,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(150),
                  color:Colors.grey.shade200,
                ),
                child: Center(
                  child: Text(
                    "$_secondsRemaining",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 120,
                      color: Colors.black,
                    ),
                  ),
                )),
          ),
        ),
      ),
    );
  }
}
