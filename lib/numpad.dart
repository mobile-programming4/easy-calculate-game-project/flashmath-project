import 'package:flutter/material.dart';

import 'const.dart';

class NumPad extends StatelessWidget {
  final String child;
  final VoidCallback onTap;
  var buttonColor = Colors.white;
  NumPad({Key? key, required this.child, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (child == 'C') {
      buttonColor = Colors.redAccent.shade100;
    } else if (child == 'OK') {
      buttonColor = Colors.greenAccent.shade100;
    }
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: buttonColor,
            ),
            child: Center(
              child: Text(
                child,
                style: fontTextStyle,
              ),
            ),
          ),
        ));
  }
}