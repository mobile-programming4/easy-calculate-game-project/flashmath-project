import 'package:column_widget_example/const.dart';
import 'package:column_widget_example/selectmode_page.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'restart_page.dart';

import 'numpad.dart';

Random random = new Random();
var timerStop;

class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  final stopwatch = Stopwatch();
  List<String> numpad = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'C',
    '0',
    'OK',
  ];
  void randomNumber() {
    firstNumber = random.nextInt(11);
    secondNumber = random.nextInt(11);
    if (operator == "-" && firstNumber - secondNumber < 0) {
      randomNumber();
    }
  }

  int countWin = 1;
  int firstNumber = -1;
  int secondNumber = random.nextInt(11);
  String operator = mode;
  //answer
  String answer = "";
  String alert = "";

  void buttontapped(String button) {
    setState(() {
      if (button == 'C') {
        answer = "";
      } else if (button == 'OK') {
        //check answer
        Checkanswer();
      } else if (answer.length < 3) {
        answer = answer + button;
      }
    });
  }

  void checkWin(int count) {
    if (count == 10) {
      stopwatch.stop();
      timerStop = stopwatch.elapsedMilliseconds / 1000;
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return RestartPage();
      }));
    }
  }

  void processCorrect() {
    randomNumber();
    countWin++;
    checkWin(countWin);
    answer = "";
    print(countWin);
  }

  void processWrong() {
    answer = "";
    alert = "WRONG";
  }

  void Checkanswer() {
    if (operator == "+") {
      if (int.parse(answer) == firstNumber + secondNumber) {
        processCorrect();
      } else {
        processWrong();
      }
    } else if (operator == "-") {
      if (int.parse(answer) == firstNumber - secondNumber) {
        processCorrect();
      } else {
        processWrong();
      }
    } else if (operator == "*") {
      if (int.parse(answer) == firstNumber * secondNumber) {
        processCorrect();
      } else {
        processWrong();
      }
    } else if (operator == "/") {
      if (int.parse(answer) == firstNumber / secondNumber) {
        processCorrect();
      } else {
        processWrong();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final height = MediaQuery.of(context).size.height;
    final smallerDeviceLanscape = height < AppLayout.widthFristBreakpoint;
    final isLanscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    if (firstNumber == -1) {
      randomNumber();
    }
    stopwatch.start();
    return Scaffold(
      backgroundColor: Color(0xFFC9FFFD),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return orientation == Orientation.landscape && smallerDeviceLanscape
              ? HorizontalLayout()
              : VerticalLayout();
        },
      ),
    );
  }

  Widget VerticalLayout() {
    final screenHeight = MediaQuery.of(context).size.height;
    return Container(
      color: Color(0xFFC9FFFD),
      child: Column(children: [
        Container(
          child: Column(
            children: [
              //score or time
              Container(
                padding: EdgeInsets.only(top: 15),
                height: screenHeight / 8.4,
                color: Color(0xFF9BAFFF),
                child: Center(
                  child: Text(
                    countWin.toString(), style: CountTextStyle,
                  ),
                ),
              ),
              //question
              Container(
                height: screenHeight / 8.4,
                padding: EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFFA0CAFD),
                  ),
                  child: Center(
                    child: Text(
                      firstNumber.toString() +
                          operator.toString() +
                          secondNumber.toString(),
                      style: labelTextStyle,
                    ),
                  ),
                ),
              ),
              Center(
                child: Text("=", style: labelTextStyle),
              ),
              //answer
              Container(
                height: screenHeight / 8.4,
                padding: EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFFA0CAFD),
                  ),
                  child: Center(
                    child: Text(
                      answer,
                      style: labelTextStyle,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        //numpad
        Container(
            height: screenHeight - ((screenHeight / 4.6) * 2),
            padding: EdgeInsets.only(top: 20, left: 20, right: 20),
            child: GridView.builder(
                itemCount: numpad.length,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 1,
                    mainAxisSpacing: 2,
                    childAspectRatio: 1.5,
                    mainAxisExtent: screenHeight / 8.4),
                itemBuilder: (context, index) {
                  return NumPad(
                      child: numpad[index],
                      onTap: () => buttontapped(numpad[index]));
                })),
      ]),
    );
  }

  Widget HorizontalLayout() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
        color: Color(0xFFC9FFFD),
        child: Row(
          children: [
            Container(
              child: Column(
                children: [
                  //score or time
                  Container(
                    width: screenWidth / 2,
                    height: (screenHeight / 8.4)*2,
                    color: Color(0xFF9BAFFF),
                    child: Center(
                      child: Text(
                        countWin.toString(), style: CountTextStyle,
                      ),
                    ),
                  ),
                  //question
                  Container(
                    width: (screenWidth / 2) - 50,
                    height: 100,
                    padding: EdgeInsets.all(15),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xFFABCFFF),
                      ),
                      child: Center(
                        child: Text(
                          firstNumber.toString() +
                              operator.toString() +
                              secondNumber.toString(),
                          style: labelTextStyle,
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text("=", style: labelTextStyle,),
                  ),
                  //answer
                  Container(
                    width: (screenWidth / 2) - 50,
                    height: 100,
                    padding: EdgeInsets.all(15),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xFFABCFFF),
                      ),
                      child: Center(
                        child: Text(
                          answer,
                          style: labelTextStyle,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Color(0xFFA0CAFD),
              child: Column(
                children: [
                  Container(
                      width: screenWidth / 2,
                      height: screenHeight,
                      padding: EdgeInsets.only(top: 5, right: 20, left: 20, bottom: 10),
                      child: GridView.builder(
                          itemCount: numpad.length,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 1,
                            mainAxisSpacing: 2,
                            childAspectRatio: 1.6,
                          ),
                          itemBuilder: (context, index) {
                            return NumPad(
                                child: numpad[index],
                                onTap: () => buttontapped(numpad[index]));
                          })),
                ],
              ),
            ),
          ],
        ));
  }
}
