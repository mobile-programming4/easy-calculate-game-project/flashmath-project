import 'package:column_widget_example/restart_page.dart';
import 'package:column_widget_example/selectmode_page.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'game_page.dart';
import 'starting_page.dart';

void main() {
  runApp(const FlashMath());
}

class FlashMath extends StatelessWidget {
  const FlashMath({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'FlashMath',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: StartingPage(),
      ),
    );
  }
}
