import 'package:column_widget_example/const.dart';
import 'package:column_widget_example/countdown_page.dart';
import 'package:column_widget_example/game_page.dart';
import 'package:flutter/material.dart';

String mode = "";

class SelectModePage extends StatelessWidget {
  const SelectModePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final smallerDeviceLanscape = height < AppLayout.widthFristBreakpoint;
    final isLanscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double paddingTop_select = 80;
    double font_select = 50;
    double paddingTop_plus = 120;
    double font_mode = 40;
    double paddingTop_mode = 50;
    double btn_mode_width = 330;
    double btn_mode_height = 70;
    return Scaffold(
      backgroundColor: Color(0xFFC9FFFD),
      body: Container(
          color: Colors.black12,
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: (smallerDeviceLanscape && isLanscape)
                        ? paddingTop_select / 2
                        : paddingTop_select),
                child: Center(
                  child: Text(
                    "SELECT MODE",
                    style: TextStyle(
                        fontSize: (smallerDeviceLanscape && isLanscape)
                            ? font_select / 1.5
                            : font_select,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Precedence'),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: (smallerDeviceLanscape && isLanscape)
                        ? paddingTop_plus / 2
                        : paddingTop_plus),
                child: Center(
                    child: InkWell(
                  borderRadius: BorderRadius.circular(45),
                  onTap: () async {
                    mode = "+";
                    await Future.delayed(Duration(seconds: 1));
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return CountDownPage();
                    }));
                  }, // Handle your callback
                  child: Ink(
                    width: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_width / 1.5
                        : btn_mode_width,
                    height: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_height / 1.5
                        : btn_mode_height,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFFADE7FF),
                    ),
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "+",
                        style: TextStyle(
                            color: Color(0xFF9C58FF),
                            fontSize: (smallerDeviceLanscape && isLanscape)
                                ? font_mode / 2
                                : font_mode,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Precedence'),
                      ),
                    ),
                  ),
                )),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: (smallerDeviceLanscape && isLanscape)
                        ? paddingTop_mode / 2
                        : paddingTop_mode),
                child: Center(
                    child: InkWell(
                  borderRadius: BorderRadius.circular(45),
                  onTap: () async {
                    mode = "-";
                    await Future.delayed(Duration(seconds: 1));
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return CountDownPage();
                    }));
                  }, // Handle your callback
                  child: Ink(
                    width: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_width / 1.5
                        : btn_mode_width,
                    height: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_height / 1.5
                        : btn_mode_height,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFFADE7FF),
                    ),
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "-",
                        style: TextStyle(
                            color: Color(0xFF9C58FF),
                            fontSize: (smallerDeviceLanscape && isLanscape)
                                ? font_mode / 2
                                : font_mode,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Precedence'),
                      ),
                    ),
                  ),
                )),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: (smallerDeviceLanscape && isLanscape)
                        ? paddingTop_mode / 2
                        : paddingTop_mode),
                child: Center(
                    child: InkWell(
                  borderRadius: BorderRadius.circular(45),
                  onTap: () async {
                    mode = "*";
                    await Future.delayed(Duration(seconds: 1));
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return CountDownPage();
                    }));
                  }, // Handle your callback
                  child: Ink(
                    width: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_width / 1.5
                        : btn_mode_width,
                    height: (smallerDeviceLanscape && isLanscape)
                        ? btn_mode_height / 1.5
                        : btn_mode_height,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFFADE7FF),
                    ),
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "x",
                        style: TextStyle(
                          color: Color(0xFF9C58FF),
                          fontSize: (smallerDeviceLanscape && isLanscape)
                              ? font_mode / 2
                              : font_mode,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                )),
              )
            ],
          )),
    );
  }
}
