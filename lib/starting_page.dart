import 'dart:io';

import 'package:column_widget_example/const.dart';
import 'package:column_widget_example/selectmode_page.dart';
import 'package:flutter/material.dart';

class StartingPage extends StatelessWidget {
  const StartingPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final smallerDeviceLanscape = height < AppLayout.widthFristBreakpoint;
    final isLanscape = MediaQuery.of(context).orientation == Orientation.landscape;
    double logo_width = 200;
    double logo_height = 200;
    double flash_font_size = 50;
    double quick_font_size = 20;
    double start_font_size = 40;
    double btn_start_width = 200;
    double btn_start_height = 70;
    double marginAll_logo = 30;
    double paddingTop_btn_start = 100;
    return Scaffold(
      backgroundColor: Color(0xFFC9FFFD),
      body: Container(
          color: Colors.black12,
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: [
                Container(
                  margin: EdgeInsets.all((smallerDeviceLanscape && isLanscape)
                      ? marginAll_logo/ 2
                      : marginAll_logo,),
                  width: (smallerDeviceLanscape && isLanscape)
                      ? logo_width / 2
                      : logo_width,
                  height: (smallerDeviceLanscape && isLanscape)
                      ? logo_height/ 2
                      : logo_height,
                  child: Center(
                      //Change Text to ImageNetwork
                      //1.
                      child: Image.network(
                          'https://media.discordapp.net/attachments/887544957345341440/1082638267486306374/LogoMathBlue.png?width=876&height=701')),
                ),
                Center(
                    child: Text(
                  "Flash Math\n     Game",
                  style: TextStyle(
                      fontSize: (smallerDeviceLanscape && isLanscape)
                          ? flash_font_size/ 2
                          : flash_font_size,
                      fontWeight: FontWeight.w900,
                      fontFamily: 'SweetsSmile'),
                )),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: Center(
                      child: Text(
                    "Q U I C K    M A T H",
                    style: TextStyle(
                        fontSize: (smallerDeviceLanscape && isLanscape)
                            ? quick_font_size/ 2
                            : quick_font_size,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'TransitCAT'),
                  )),
                ),
                Padding(
                  padding: EdgeInsets.only(top: (smallerDeviceLanscape && isLanscape)
                      ? paddingTop_btn_start/ 2
                      : paddingTop_btn_start,),
                  child: Center(
                      child: InkWell(
                    borderRadius: BorderRadius.circular(45),
                    onTap: () async {
                      await Future.delayed(Duration(seconds: 1));
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return SelectModePage();
                      }));
                    }, // Handle your callback
                    child: Ink(
                      width:(smallerDeviceLanscape && isLanscape)
                          ? btn_start_width/ 2
                          : btn_start_width,
                      height: (smallerDeviceLanscape && isLanscape)
                          ? btn_start_height/ 2
                          : btn_start_height,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: Color(0xFFADE7FF),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "START",
                          style: TextStyle(
                              color: Color(0xFF809EFF),
                              fontSize: (smallerDeviceLanscape && isLanscape)
                                  ? start_font_size/ 2
                                  : start_font_size,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Precedence'),
                        ),
                      ),
                    ),
                  )),
                )
              ],
            ),
          )),
    );
  }
}
