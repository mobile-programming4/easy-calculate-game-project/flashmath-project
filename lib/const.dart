import 'package:flutter/material.dart';

var labelTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 42,
  color: Color(0xFF5977FF),
    fontFamily: 'Precedence'
);

var CountTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 42,
  color: Colors.white,
  fontFamily: 'Precedence'
);

var fontTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 30,
  color: Colors.grey[800],
  fontFamily: 'Precedence'
);

abstract class AppLayout{
  static const double widthFristBreakpoint = 500.0;
}


