import 'dart:io';
import 'package:column_widget_example/const.dart';
import 'package:column_widget_example/selectmode_page.dart';
import 'package:column_widget_example/starting_page.dart';
import 'package:flutter/material.dart';
import 'game_page.dart';

class RestartPage extends StatelessWidget {
  const RestartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //responsive
    final height = MediaQuery.of(context).size.height;
    final smallerDeviceLanscape = height < AppLayout.widthFristBreakpoint;
    final isLanscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double marginAll_time = 40;
    double font_time = 65;
    double font_finishTime = 80;
    double paddingTop_refresh = 80;
    double container_time = 150;
    double size_icon_refresh = 100;
    var Finistime = timerStop ~/ 1;
    var Finishtime_minute = Finistime ~/ 60;
    var Finishtime_second = Finistime % 60;
    var stringminute = "";
    var stringsecond = Finishtime_second;
    if (Finishtime_minute < 10) {
      stringminute = "0" + Finishtime_minute.toString();
    } else {
      stringminute = Finishtime_minute.toString();
    }
    if (Finishtime_second < 10) {
      stringsecond = "0" + Finishtime_second.toString();
    } else {
      stringsecond = Finishtime_second.toString();
    }
    return Scaffold(
      backgroundColor: Color(0xFFC9FFFD),
      body: Container(
        color: Colors.black12,
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all((smallerDeviceLanscape && isLanscape)
                  ? marginAll_time / 2
                  : marginAll_time),
              width: (smallerDeviceLanscape && isLanscape)
                  ? container_time / 2
                  : container_time,
              height: (smallerDeviceLanscape && isLanscape)
                  ? container_time / 2
                  : container_time,
              child: Center(
                //Change Text to ImageNetwork
                //1.
                child: Text("TIME",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: (smallerDeviceLanscape && isLanscape)
                            ? font_time / 1.5
                            : font_time,
                        fontFamily: 'TransitCAT')),
              ),
            ),
            Center(
              child: Text(
                stringminute + ":" + stringsecond,
                style: TextStyle(
                    fontSize: (smallerDeviceLanscape && isLanscape)
                        ? font_finishTime / 1.5
                        : font_finishTime,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Center(
              child: Container(
                height: 20,
                width: 800,
                child: Divider(
                  color: Colors.black,
                  indent: 50,
                  endIndent: 50,
                  thickness: 5,
                ),
              ),
            ),
            // Container(
            //     margin: EdgeInsets.all(50),
            //     width: 200,
            //     height: 150,
            //     child: Center(
            //       child: Text(
            //        timerStop.toString(),
            //         style: TextStyle(
            // fontSize: 50,
            // color: Colors.transparent,
            // shadows: [Shadow(offset: Offset(0, -20), color: Colors.black)],
            // decoration: TextDecoration.underline,
            // decorationColor: Colors.redAccent,
            //   ),
            // ),
            // )),
            Padding(
              padding: EdgeInsets.only(
                  top: (smallerDeviceLanscape && isLanscape)
                      ? paddingTop_refresh / 2
                      : paddingTop_refresh),
              child: Center(
                  child: InkWell(
                borderRadius: BorderRadius.circular(100),
                onTap: () async {
                  await Future.delayed(Duration(seconds: 1));
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return StartingPage();
                  }));
                }, // Handle your callback
                child: Ink(
                  width: (smallerDeviceLanscape && isLanscape)
                      ? container_time / 1.5
                      : container_time,
                  height: (smallerDeviceLanscape && isLanscape)
                      ? container_time / 1.5
                      : container_time,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: Color(0xFF8EE2FF),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: Icon(Icons.loop,
                        size: (smallerDeviceLanscape && isLanscape)
                            ? size_icon_refresh / 1.5
                            : size_icon_refresh),
                  ),
                ),
              )),
            )
          ],
        ),
      ),
    );
  }
}
